all: hello
 
# $@ matches the target; $< matches the first dependent
hello: hello1.o hello2.o
	gcc -shared -W -o libshared.so $^

hello1.o: hello1.c
	gcc -c $<

hello2.o: hello2.c
	gcc -c $<

clean:
	rm hello1.o hello2.o libshared.so
